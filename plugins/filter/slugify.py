#!/usr/bin/python

from ansible.errors import AnsibleFilterError
import re

DOCUMENTATION = r"""
name: gxmmx.utility.variable
short_description: read file contents
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - This lookup returns the contents from a file on the Ansible control node's file system.

options:
  _value:
    description:
      - Variable to validate.
    required: True
  lower:
    description:
      - Ensures slug is lowercase.
    type: bool
    required: False
  delimiter:
    description:
      - Delimiter to replace characters with.
    type: str
    default: '-'
    required: False
  replace:
    description:
      - Comma separated string of replacements.
      - Takes the form 'replace_this:with_this'
    type: str
    required: False
"""

# from ansible_collections.gxmmx.utility.plugins.module_utils.validator.validator import Validator

# ------------------------------------------------------------------------------
# Variable Filter
# ------------------------------------------------------------------------------


class FilterModule(object):
    def filters(self):
        return {
            'slugify': self.slugify
        }

    def slugify(self, value, lower=True, delimiter='-', replace=None):
        # Argument validation
        if not isinstance(value, (int, str)):
             raise AnsibleFilterError(f"Filter can not process value of type {type(value).__name__}.")
        if not isinstance(lower, bool):
             raise AnsibleFilterError(f"Filter 'lower' value must be of type bool.")
        if not isinstance(delimiter, str):
             raise AnsibleFilterError(f"Filter 'delimiter' value must be of type string.")

        slug = str(value)
        pattr = r'[^A-Za-z0-9]+'

        # Replacements
        if replace:
            if not isinstance(replace, str):
                raise AnsibleFilterError(f"Filter 'replacement' value must be of type string.")
            replacements = replace.split(',')
            for replacement in replacements:
                parts = replacement.split(':')
                if len(parts) != 2:
                     raise AnsibleFilterError(f"Filter 'replacement' value invalid.")
                slug = re.sub(re.escape(parts[0]), parts[1], slug)

        # Lower
        if lower:
            slug = slug.lower()
            pattr = r'[^a-z0-9]+'

        # process
        slug = re.sub(pattr, delimiter, slug)
        slug = re.sub(re.escape(delimiter) + r'+', delimiter, slug)
        slug = slug.strip(delimiter)

        return slug
