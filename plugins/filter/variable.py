#!/usr/bin/python

from ansible.errors import AnsibleError

DOCUMENTATION = r"""
name: variable
short_description: read file contents
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - This lookup returns the contents from a file on the Ansible control node's file system.

options:
  _value:
    description:
      - Variable to validate.
    required: True
  schema:
    description:
      - Sample option that could modify plugin behavior.
      - This one can be set directly ``option1='x'`` or in ansible.cfg, but can also use vars or environment.
    type: dict
    required: True
"""

from ansible_collections.gxmmx.utility.plugins.module_utils.validator.validator import Validator

# ------------------------------------------------------------------------------
# Variable Filter
# ------------------------------------------------------------------------------

class FilterModule(object):
    def filters(self):
        return {
            'variable': self.validate_variable
        }

    def validate_variable(self, value, name="variable", **schema):
        
        validator_var = {name: value}
        validator_schema = {name: schema}

        validator = Validator(validator_var, validator_schema)
        validator.validate()
        if not validator.is_valid():
            msg = f"{{'errors': {validator.get_errors()}, 'msg': { validator.get_error_message()}}}"
            raise AnsibleError(msg)

        return value
