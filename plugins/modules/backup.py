#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
module: backup
short_description: Manages backup files
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - Manages backup files created by Ansibles template and file modules.
  - Restore latest or specific backup.
  - Ensures rotation/removal of oldest backups.

options:
  path:
    description: Path of backed up file.
    required: true
    type: str
  restore:
    description: Restores either a specific backup or 'latest' or 'none'
    required: false
    type: str
    default: none
  keep:
    description: Backups to keep, Latest will be retained.
    required: false
    type: int

attributes:
  check_mode:
    support: full
  diff_mode:
    support: none

extends_documentation_fragment:
  - ansible.builtin.files
'''

EXAMPLES = r'''
- name: Ensure only 5 latest backups are retaind
  gxmmx.utility.backup:
    path: /path/to/file.conf
    keep: 5

- name: Restore latest backup
  gxmmx.utility.backup:
    path: /path/to/file.conf
    restore: latest
  when: some_issue_bool

- name: Restore specific backup
  gxmmx.utility.backup:
    path: /path/to/file.conf
    restore: template_registered.backup_file
  when: some_issue_bool

- name: restore specific backup when issue, but always run for retention
  gxmmx.utility.backup:
    path: /path/to/file.conf
    restore: "{{ template_registered.backup_file if some_issue else 'none' }}"
    keep: 5
'''

RETURN = r'''
cleanup:
    description: Indicates if cleanup was performed.
    type: bool
    returned: always
    sample: true
restored:
    description: Indicates if restore was performed.
    type: bool
    returned: always
    sample: true
restored_file:
    description: Path of file restored
    type: str
    returned: when restored
    sample: '/path/to/file.conf.1234.2024-01-18@12:23:45~'
'''

from ansible.module_utils.basic import AnsibleModule

from ansible_collections.gxmmx.utility.plugins.module_utils.backup.backup import BackupFiles

# ------------------------------------------------------------------------------
# Token Module
# ------------------------------------------------------------------------------

def main():
    module_args = dict(
        path=dict(type='str', required=True),
        keep=dict(type='int', default=100, required=False),
        restore=dict(type='str', required=False, default="none")
    )

    result = dict(
        changed = False,
        restored = False,
        cleanup = 0,
        restored_file = ''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Fetch backups
    try:
        backups = BackupFiles(module.params['path'], module.check_mode)
    except Exception as e:
        module.fail_json(msg=e, **result)

    # Restore backup
    try:
        restored = backups.restore(module.params['restore'])
    except Exception as e:
        module.fail_json(msg=e, **result)

    if restored:
        result['restored_file'] = restored
        result['restored'] = True
        result['changed'] = True

    # Cleanup backups
    try:
        cleaned = backups.cleanup(module.params['keep'])
    except Exception as e:
        module.fail_json(msg=e, **result)

    if cleaned:
        result['cleanup'] = cleaned
        result['changed'] = True

    # Exit on success
    module.exit_json(**result)


if __name__ == '__main__':
    main()
