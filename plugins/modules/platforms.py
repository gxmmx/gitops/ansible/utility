#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
module: platforms
short_description: Platform support
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - This module handles distribution support.
  - Validation is performed of current platform against a schema.
  - Platform specific variables are loaded from vars/os_dist_ver.yml
  - Note: Module has corresponding action plugin.

options:
  schema:
    description:
      - The platform schema for available platform names and versions.
      - This uses the format as per galaxy_info in role meta.
      - This field can be passed the schema list, or 'role_meta' to fetch from meta.
    type: raw
    required: True

attributes:
  check_mode:
    support: full
  diff_mode:
    support: none
'''

EXAMPLES = r'''
- name: Validate current host is supported by galaxy_info meta
  gxmmx.utility.platforms:
    schema: role_meta

- name: Validate current host is supported by schema
  gxmmx.utility.platforms:
    schema:
      - name: CentOS
        versions: ['all']
      - name: Debian
        versions: ['10', '11']
'''

RETURN = r'''
'''

from ansible.module_utils.basic import AnsibleModule

# ------------------------------------------------------------------------------
# Platforms Module
# ------------------------------------------------------------------------------

def main():
    module_args = dict(
        schema=dict(type='raw', required=True),
        _platform_vars=dict(type='dict', required=True)
    )

    result = dict(
        changed=False,
        ansible_facts={}
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Module has a corresponding action plugin handles action of module.
    # Current distro will be validated against platform schema.

    # Set the loaded platform specific variables as added facts.
    result['ansible_facts'] = module.params['_platform_vars']

    module.exit_json(**result)


if __name__ == '__main__':
    main()
