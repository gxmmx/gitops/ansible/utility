#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
module: token
short_description: Manages tokens
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - This module handles local token generation and retrieval from multiple sources.
  - Token retrieval is precedence based.
  - Passed by variable (token), environment variable, file at path or generated.
  - It can handle generation only, retrieval only, or multipurposed for generation and subsequent retrieval.

options:
  value:
    description: Token value
    type: string
    required: False
  env:
    description: Environment variable name
    type: string
    required: False
    aliases: envvar
  vault:
    description: Vault path (Inactive: Not implemented yet)
    type: str
    required: False
  path:
    description: Token file path
    type: str
    required: False
    aliases: file
  generate:
    description: Generate Token if not passed through above methods
    type: bool
    required: False
    default: False
  length:
    description: Generated token length
    type: int
    required: False
    default: 32
  format:
    description: Generated token format
    type: str
    required: False
    default: 'alnum'
  prefix:
    description: Generated token prefix value.
    type: str
    required: False
    default: ''
  save:
    description: Generated token will be saved if a path is provided
    type: bool
    required: False
    default: False
  fail:
    description: Determines if module should fail upon unsuccessful token retrieval.
    type: bool
    required: False
    default: True
  missing:
    description: Sets custom message if module is to fail.
    type: str
    required: False
    default: 'Token missing'

attributes:
  check_mode:
    support: full
  diff_mode:
    support: none

extends_documentation_fragment:
  - ansible.builtin.files
'''

EXAMPLES = r'''
- name: Generate a token
  gxmmx.utility.token:
    generate: true

- name: Generate a 24 char token using uppercase and numeric with a prefix
  gxmmx.utility.token:
    generate: true
    length: 24
    format: 'uppnum'
    prefix: 'mytoken_'

- name: Generate a token and save to file
  gxmmx.utility.token:
    path: /dir/token_name
    save: true
    mode: 0600
    generate: true

- name: Populate a token from multiple sources
  gxmmx.utility.token:
    env: "TOKEN_NAME"
    path: /dir/token_name
    save: false

- name: Initially generate token, then fetch from file on subsequent runs
  gxmmx.utility.token:
    path: /dir/token_name
    save: true
    mode: '0600'
    generate: true
'''

RETURN = r'''
value:
    description: The token string.
    type: str
    returned: always
    sample: 'token_value'
source:
    description: The tokens source
    type: str
    returned: always
    sample: 'env'
path:
    description: The file path for token read and write.
    type: str
    returned: If file action is performed
    sample: '/dir/token'
'''

from ansible.module_utils.basic import AnsibleModule

from ansible_collections.gxmmx.utility.plugins.module_utils.token.generator import TokenGenerator

import os

# ------------------------------------------------------------------------------
# Token Module
# ------------------------------------------------------------------------------

def main():
    module_args = dict(
        token=dict(type='str', required=False),
        env=dict(type='str', required=False, aliases=['envvar']),
        path=dict(type='str', required=False, aliases=['file']),
        save=dict(type='bool', required=False, default=False),
        fail=dict(type='bool', required=False, default=True),
        missing=dict(type='str', required=False, default="Token missing"),
        generate=dict(type='bool', required=False, default=True),
        length=dict(type='int', required=False, default=32),
        prefix=dict(type='str', required=False, default=""),
        format=dict(type='str', required=False, default='alnum', choices=['number','lower','upper','alpha','alnum','uppnum','lownum'])
    )

    result = dict(
        changed = False,
        exists = False,
        source = '',
        path = '',
        value = ''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        required_one_of=[('token', 'env', 'path', 'generate')],
        required_if = [('save', True, ('path',))],
        supports_check_mode=True,
        add_file_common_args=True
    )


    # Validation
    no_token_set = True
    gen_min_length = 8
    gen_max_length = 128

    # Set token
    if module.params['token']:
        result['value'] = module.params['token']
        result['source'] = 'value'
        result['exists'] = True
        no_token_set = False

    if no_token_set and module.params['env']:
        if module.params['env'] in os.environ:
            result['value'] = os.environ.get( module.params['env'])
            result['source'] = 'environment'
            result['exists'] = True
            no_token_set = False

    if no_token_set and module.params['path']:
        path = os.path.expanduser(module.params['path'])

        if os.path.isfile(path):
            try:
                with open(path, "r") as f:
                    content = f.read()
                result['value'] = content.strip()
                result['source'] = 'file'
                result['exists'] = True
                result['path'] = path
                no_token_set = False
            except:
                module.fail_json(msg='Could not read token from file: ' + path, **result)

    if no_token_set and module.params['generate']:
        if module.params['length'] < gen_min_length or module.params['length'] > gen_max_length:
            module.fail_json(msg=f"Token length must be between {gen_min_length}-{gen_max_length}", **result)

        token = TokenGenerator(module.params['length'], module.params['format'], module.params['prefix'])
        result['value'] = token.value()
        result['source'] = 'generated'
        result['exists'] = True
        no_token_set = False

    if no_token_set:
        if module.params['fail']: 
            module.fail_json(msg=module.params['missing'], **result)
        else:
            module.exit_json(**result)

    # Save token
    if module.params['save']:
        path = os.path.expanduser(module.params['path'])
        dir =  os.path.dirname(path)

        if not os.path.isdir(dir):
            module.fail_json(msg='Directory for saving token does not exists.', **result)

        if not module.check_mode:
            if not os.path.isfile(path):
                try:
                    with open(path, "w") as f:
                        f.write(result['value'])
                        result['changed'] = True
                except:
                    module.fail_json(msg='Could not create token', **result)
            else:
                try:
                    with open(path, "r+") as f:
                        current_token =  f.read().rstrip()
                        if current_token != result['value']:
                            f.seek(0)
                            f.write(result['value'])
                            f.truncate()
                            result['changed'] = True
                except:
                    module.fail_json(msg='Could not overwrite token', **result)

        file_args = module.load_file_common_arguments(module.params)
        result['changed'] = module.set_fs_attributes_if_different(file_args, result['changed'])

    module.exit_json(**result)


if __name__ == '__main__':
    main()
