#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
module: variables
short_description: Validates variables against schema
author:
  - Gudmundur Gudmundsson (@gummigudm)
version_added: "1.0.0"

description:
  - This module validates passed variables against a schema to ensure integrity.
  - Works much like Ansibles argument_specs, but pluggable.
  - The module recursively validates all passed objects and throws an error if any parameters fail.
  - Note: Module has corresponding action plugin.

options:
  schema:
    description:
      - Variable schema for validation, or a reference to meta schema.
      - The schema is similar to Ansibles argument_specs, though there are differences.
      - See documentation for details on format of schema.
      - This field can be passed the schema dictionary, or 'role_meta' to fetch from meta.
    type: raw
    required: True

attributes:
  check_mode:
    support: full
  diff_mode:
    support: none
'''

EXAMPLES = r'''
- name: Validate variables from roles meta/variables.yml
  gxmmx.utility.variables:
    schema: role_meta

- name: Validate variables using variable as schema
  gxmmx.utility.variables:
    schema: "{{ my_schema }}"

- name: Validate variables using a direct schema
  gxmmx.utility.variables:
    schema:
      my_integer_var:
        type: integer
      my_string_var:
        type: string
'''

RETURN = r'''
errors:
  description: List of validation errors
  type: list
  returned: on error
'''

from ansible.module_utils.basic import AnsibleModule

from ansible_collections.gxmmx.utility.plugins.module_utils.validator.validator import Validator

import os

# ------------------------------------------------------------------------------
# Validator Module
# ------------------------------------------------------------------------------

def main():
    module_args = dict(
        schema=dict(type='raw', required=True),
        _vars=dict(type='dict', required=True),
        _schema=dict(type='dict', required=True)
    )

    result = dict(
        changed = False,
        errors = [],
        doc = []
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
        #add_file_common_args=True
    )

    # Set variables
    vars = module.params['_vars']
    schema = module.params['_schema']

    # Run validator
    validator = Validator(vars, schema)
    validator.validate()
    if not validator.is_valid():
        result['errors'] = validator.get_errors()
        message = validator.get_error_message()
        module.fail_json(msg=message, **result)

    module.exit_json(**result)

    # # If path was passed, run documentation
    # validator.document()
    # if not validator.is_valid():
    #     result['errors'] = validator.get_errors()
    #     message = validator.get_error_message()
    #     module.fail_json(msg=message, **result)

    # doc_lines = validator.get_documentation()

    # path = module.params['path']
    # doc_begin = module.params['doc_begin']
    # doc_end = module.params['doc_end']

    # if not os.path.exists(path):
    #     if module.check_mode:
    #         result['changed'] = True
    #         module.exit_json(**result)
    #     content = f"{doc_begin}\n{doc_end}\n"
    #     try:
    #         with open(path, 'w') as f:
    #             f.write(content)
    #     except:
    #         message = f"Failed writing documentation to file: {path}"
    #         module.fail_json(msg=message, **result)

    # try:
    #     with open(path, 'r') as f:
    #         lines = f.readlines()
    # except:
    #     module.fail_json(msg=f"Failed reading file: {path}")

    # begin_index = None
    # end_index = None

    # for index, line in enumerate(lines):
    #     if line.strip() == doc_begin:
    #         begin_index = index
    #     elif line.strip() == doc_end:
    #         end_index = index

    # if begin_index is None or end_index is None or begin_index >= end_index:
    #     message = f"Invalid markers in file: {path}"
    #     module.fail_json(msg=message, **result)

    # current_content = lines[begin_index+1:end_index]
    # new_content = [r"```yaml" + '\n'] + [line + '\n' for line in doc_lines] + [r"```" + '\n']

    # if current_content != new_content:
    #     result['changed'] = True
    #     if not module.check_mode:
    #         lines[begin_index+1:end_index] = new_content
    #         with open(path, 'w') as f:
    #             f.writelines(lines)

    # file_args = module.load_file_common_arguments(module.params)
    # result['changed'] = module.set_fs_attributes_if_different(file_args, result['changed'])
    # module.exit_json(**result)


if __name__ == '__main__':
    main()
