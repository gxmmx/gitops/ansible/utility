from ansible.plugins.action import ActionBase
import yaml
import os
import re

# ------------------------------------------------------------------------------
# Platforms Action
# ------------------------------------------------------------------------------

class ActionModule(ActionBase):

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        # Ensure facts
        distribution = task_vars.get('ansible_distribution')
        major_version = task_vars.get('ansible_distribution_major_version')

        if not distribution:
            return {"failed": True, "msg": f"Could not get hosts distribution. Module requires facts to be gathered."}
        if not major_version:
            return {"failed": True, "msg": f"Could not get hosts major version. Module requires facts to be gathered."}

        # Get schema from module
        platforms = []
        schema = self._task.args.get('schema')
        if not schema:
            return {"failed": True, "msg": "Missing required arguments: schema"}

        if isinstance(schema, str):
            if schema == "role_meta":
                role_path = task_vars.get('role_path')
                if not role_path:
                    return {"failed": True, "msg": "Could not get schema from role_meta. Task not run from role."}
                role_meta = os.path.join(role_path, 'meta/main.yml')
                try:
                    meta_schema = self._load_yaml_from_file(role_meta)
                except Exception as e:
                    return {"failed": True, "msg": f"Could not get schema from role meta: {e}"}
                try:
                    platforms = self._get_platforms_from_meta(meta_schema)
                except Exception as e:
                    return {"failed": True, "msg": f"Invalid schema: {e}"}
            else:
                return {"failed": True, "msg": f"Invalid schema: {schema}"}

        elif isinstance(schema, list):
            try:
                platforms = self._valid_platforms(schema)
            except Exception as e:
                return {"failed": True, "msg": f"Invalid schema: {e}"}
        else:
            return {"failed": True, "msg": "Argument 'schema' invalid. Must be of type 'str' or 'list'"}

        # Validate platform
        if not self._allowed_platform(str(distribution), str(major_version), platforms):
            return {"failed": True, "msg": f"Platform unsupported: {str(distribution)} {str(major_version)}"}

        # Find platform variables
        platform_name_ver = f"os_{self._name_to_slug(str(distribution))}_{self._name_to_slug(str(major_version))}.yml"
        platform_name_all = f"os_{self._name_to_slug(str(distribution))}.yml"
        try:
            vars_file_path = self._find_needle('vars', platform_name_ver)
        except:
            try:
                vars_file_path = self._find_needle('vars', platform_name_all)
            except:
                return {"failed": True, "msg": f"Platform specific variables not found under '{platform_name_ver}' or {platform_name_all}"}
        
        try:
            platform_vars = self._load_yaml_from_file(vars_file_path)
        except Exception as e:
            return {"failed": True, "msg": f"Platform specific variable error: {e}"}

        # Pass loaded vars to the module
        result = self._execute_module(
            module_name='platforms',
            module_args={
                'schema': platforms,
                '_platform_vars': platform_vars
            },
            task_vars=task_vars
        )
        return result


# ------------------------------------------------------------------------------
# Internal functions
# ------------------------------------------------------------------------------

    # Loads variables from yaml file
    def _load_yaml_from_file(self, file_path):
        try:
            with open(file_path, 'r') as file:
                file_content = file.read()
        except:
            raise Exception(f"Could not read file: {file_path}")
        try:
            yaml_content = yaml.safe_load(file_content)
        except:
            raise Exception(f"Could not parse file: {file_path}")
        return yaml_content

    # Parse galaxy vars from meta/main.yml and return platforms
    def _get_platforms_from_meta(self, meta_vars: dict):
        platforms = []
        if not meta_vars.get('galaxy_info'):
            raise Exception("Platforms not found in role_meta. Expecting 'galaxy_info' in main.yml")
        if not meta_vars['galaxy_info'].get('platforms'):
            raise Exception("Platforms not found in role_meta. Expecting 'platforms' in 'galaxy_info' in main.yml")
        found_platforms = meta_vars['galaxy_info'].get('platforms')
        try:
            platforms = self._valid_platforms(found_platforms)
        except Exception as e:
            raise Exception(f"Invalid platforms: {e}")
        return platforms

    # Validate platforms passed
    def _valid_platforms(self, platforms):
        if not isinstance(platforms, list):
            raise Exception("Platform data invalid. Expecting type 'list'")
        for platform in platforms:
            # Validate
            if not isinstance(platform, dict):
                raise Exception("Platform entry invalid. Expecting type 'dict'")
            if 'name' not in platform or not platform['name']:
                raise Exception("Platform entry invalid. Key 'name' missing or empty")
            if not isinstance(platform['name'], str):
                raise Exception("Platform entry invalid. Key 'name' must be of type 'str'")
            if 'versions' not in platform or not platform['versions']:
                raise Exception("Platform entry invalid. Key 'versions' missing or empty")
            if not isinstance(platform['versions'], list):
                raise Exception("Platform entry invalid. Key 'versions' must be of type 'list'")
            if not all(isinstance(version, (int, str)) for version in platform['versions']):
                raise Exception("Platform entry invalid. Key 'versions' values must be of type 'str' or 'int'")
        return platforms

    # Validate current is in platforms
    def _allowed_platform(self, name: str, version: str, platforms: list):
        for platform in platforms:
            versions = [str(v).lower() for v in platform['versions']]
            if platform.get('name').lower() == name.lower():
                if 'all' in versions or version.lower() in versions:
                    return True
        return False

    # Slugify name for file handling
    def _name_to_slug(self, name: str):
        pattr = r'[^a-z0-9]+'
        delimiter = r'_'
        slug = re.sub(pattr, delimiter, name.lower())
        slug = re.sub(re.escape(delimiter) + r'+', delimiter, slug)
        slug = slug.strip(delimiter)
        return slug




        ##### RELATIVE PATH MATCHING FOR DOCS...
        # # Your base path
        # long_path = os.path.dirname(schema_path)

        # # You want to go back two directories and then into 'doc/filename'
        # relative_path = "../../doc/filename"

        # # Combine the paths
        # new_path = os.path.join(long_path, relative_path)

        # # Normalize the path to resolve the '..' parts
        # resulting_path = os.path.normpath(new_path)

        # # print(resulting_path)  # Output: /var/some/dir/that/doc/filename

        ##### GET ALL VARS IN ACTION PLUGIN
        # # myvar = task_vars.get('vars')
        # # return {"failed": True, "msg": f"Debug fail: {myvar}"}
