from ansible.plugins.action import ActionBase
import yaml
import os

# ------------------------------------------------------------------------------
# Platforms Action
# ------------------------------------------------------------------------------

class ActionModule(ActionBase):

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        # All variables available to Ansible
        _all_vars = task_vars.get('vars')

        # Get schema from module
        schema = self._task.args.get('schema')
        if not schema:
            return {"failed": True, "msg": "Missing required arguments: schema"}

        if isinstance(schema, str):
            if schema == "role_meta":
                role_path = task_vars.get('role_path')
                if not role_path:
                    return {"failed": True, "msg": "Could not get schema from role_meta. Task not run from role."}
                role_meta = os.path.join(role_path, 'meta/variables.yml')
                try:
                    variable_schema = self._load_yaml_from_file(role_meta)
                except Exception as e:
                    return {"failed": True, "msg": f"Could not get schema from role meta: {e}"}
            else:
                return {"failed": True, "msg": f"Invalid schema: {schema}"}

        elif isinstance(schema, dict):
            variable_schema = schema
        else:
            return {"failed": True, "msg": "Argument 'schema' invalid. Must be of type 'str' or 'dict'"}

        # Pass loaded schema and vars to the module
        result = self._execute_module(
            module_name='gxmmx.utility.variables',
            module_args={
                'schema': schema,
                '_vars': _all_vars,
                '_schema': variable_schema
            },
            task_vars=task_vars
        )
        return result


# ------------------------------------------------------------------------------
# Internal functions
# ------------------------------------------------------------------------------

    # Loads variables from yaml file
    def _load_yaml_from_file(self, file_path):
        try:
            with open(file_path, 'r') as file:
                file_content = file.read()
        except:
            raise Exception(f"Could not read file: {file_path}")
        try:
            yaml_content = yaml.safe_load(file_content)
        except:
            raise Exception(f"Could not parse file: {file_path}")
        return yaml_content
