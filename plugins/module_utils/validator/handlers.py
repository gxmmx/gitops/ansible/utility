# ------------------------------------------------------------------------------
# Log Handler
# ------------------------------------------------------------------------------

class LogHandler:
    """
    A singleton class for managing logging of validator
    """
    _instance = None
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(LogHandler, cls).__new__(cls)
            cls.errors = []
        return cls._instance

    def error(self, msg: str, var: str):
        self.errors.append({"var": var, "msg": msg})

    def all_errors(self) -> list:
        return self.errors

    def error_message(self) -> str:
        count = len(self.errors)
        if count == 0:
            message = "Validation passed"
        if count == 1:
            message = "Validation failed with an error"
        else:
            message = f"Validation failed with {count} errors"
        return message

    def has_errors(self) -> bool:
        return bool(self.errors)

# ------------------------------------------------------------------------------
# Variable Handler
# ------------------------------------------------------------------------------
# TODO: Change to handle schema as string (name) or passed dictionary schema
class VariableHandler:
    """
    A singleton class for managing passed variables and schema
    """
    _instance = None
    _valid_schema = False
    def __new__(cls, vars: dict=None, schema=None):
        if cls._instance is None:
            cls._instance = super(VariableHandler, cls).__new__(cls)
            cls._instance.initialize(vars, schema)
        return cls._instance

    def initialize(self, vars: dict=None, schema=None):
        # Ensure propper init
        if vars is None:
            raise Exception("Variable handler has not been initialized with variables")
        if schema is None:
            raise Exception("Variable handler has not been initialized with a schema")

        # Set details
        self.log = LogHandler()
        self.vars = vars
        self.schema = None

        # Set schema
        if isinstance(schema, str):
            if schema not in self.vars:
                self.log.error("Validation schema not found in passed variables", schema)
                return None
            if not isinstance(self.vars[schema], dict):
                self.log.error("Validation schema is not a valid dictionary", schema)
                return None
            if not self.vars[schema]:
                self.log.error("Validation schema is empty", schema)
                return None
            self.schema = self.vars[schema]
            self._valid_schema = True
        elif isinstance(schema, dict):
            if not schema:
                self.log.error("Validation schema is empty", schema)
                return None
            self.schema = schema
            self._valid_schema = True
        else:
            self.log.error("Validation schema is of invalid type", type(schema).__name__)

    def get_var(self, var_name):
        if var_name in self.vars:
            return self.vars.get(var_name)
        else:
            return None

    def get_schema(self) -> list:
        if self._valid_schema:
            return dict(self.schema).items()
        else:
            return None

# ------------------------------------------------------------------------------
# Doc Handler
# ------------------------------------------------------------------------------

class DocHandler:
    """
    A singleton class for managing documentation of validator
    """
    _instance = None
    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DocHandler, cls).__new__(cls)
            cls.doc_dict = {}
            cls.doc_list = []
        return cls._instance

    def add_entry(self, name: str, data: dict):
        self.doc_dict[name] = data

    def doc_order(self, passed_schema: dict, indent: int=0, deactivate=False):
        doc = []

        for _, schema in passed_schema.items():
            data = {
                "indent": indent,
                "name": schema['name'],
                "type": schema['type'],
                "order": schema['order'],
                "active": True if indent == 0 else (False if deactivate else schema['active']),
                "value": schema['value'],
                "parameters": schema['parameters'],
                "description": schema['description']
            }

            if 'item' in schema:
                list_schema = {"item": schema['item']}
                sub_deactivate = True if deactivate or schema['value'] != "schema" or not schema['active'] else False
                inner_list = self.doc_order(list_schema, indent + 1, sub_deactivate)
                data['children'] = inner_list
            elif 'keys' in schema:
                dict_schema = schema['keys']
                sub_deactivate = True if deactivate or schema['value'] != "schema" or not schema['active'] else False
                inner_dict = self.doc_order(dict_schema, indent + 1, sub_deactivate)
                data['children'] = inner_dict

            doc.append(data)
        doc.sort(key=lambda x: (x["order"], x["name"]))
        return doc

    def doc_flatten(self, passed_list: list):
        for item in passed_list:
            data = {k: v for k, v in item.items() if k != "children"}
            self.doc_list.append(data)

            if 'children' in item:
                self.doc_flatten(item['children'])


    def doc_gen_vars(self, passed_list: list, fli=False):
        vars = []

        for list_item in passed_list:
            lines = []
            name = list_item.get('name')
            value = list_item.get('value')
            indent = list_item.get('indent')
            # write comment lines
            lines.append(['## -------------------------------------','c'])
            if indent == 0:
                lines.append([f'## {name}','c'])
                lines.append(['##','c'])
            for desc in list_item.get('description'):
                lines.append([f'# {desc}','c'])
            if list_item.get('description'):
                lines.append(['##','c'])
            lines.append([f'# Type: {list_item.get("type")}','c'])
            for param in list_item.get('parameters'):
                for key, val in param.items():
                    lines.append([f'# {key}: {val}','c'])
            lines.append(['##','c'])
            # Write empty line
            lines.append(['','e'])

            # Varline
            if list_item.get('type') not in ['list', 'dictionary', 'boolean', 'integer']:
                value = f"\"{value}\""

            varline = f"{name}: {value}"

            if list_item.get('type') in ["list", "dictionary"] and value == "schema":
                varline = f"{name}:"

            if name == "item" and list_item.get('type') == "dictionary":
                varline = ""

            if name == "item" and list_item.get('type') != "dictionary":
                varline = f"- {value}"

            if varline:
                lines.append([varline, "v"])
                lines.append(["","e"])

            if indent:
                ident = '  ' * indent
                for line in lines:
                    l = line[0]
                    if line[1] == "c":
                        line[0] = f"{ident}{l}"
                    if line[1] == "v":
                        if fli:
                            ident = '  ' * (indent - 1)
                            line[0] = f"{ident}- {l}"
                            fli = False
                        else:
                            line[0] = f"{ident}{l}"

                        if not list_item.get('active'):
                            l = line[0]
                            line[0] = f"#{l[1:]}"

            if name == "item" and list_item.get('type') == "dictionary":
                fli = True

            var_lines = [line[0] for line in lines]
            vars.extend(var_lines)
            # Recurse
            if list_item.get('children'):
                vars.extend(self.doc_gen_vars(list_item.get('children'), fli))

        return vars


    def generate(self):
        doc_ordered = self.doc_order(self.doc_dict)
        vars = self.doc_gen_vars(doc_ordered)
        return vars
