from ansible_collections.gxmmx.utility.plugins.module_utils.validator.handlers import LogHandler, VariableHandler, DocHandler
from ansible_collections.gxmmx.utility.plugins.module_utils.validator.types import TypeValidator

# ------------------------------------------------------------------------------
# Validator
# ------------------------------------------------------------------------------

class Validator:
    """
    The top level class that handles validation of variables based on a schema.
    """
    def __init__(self, vars: dict, schema=None):
        self.log = LogHandler()
        self.doc = DocHandler()
        self.vars = VariableHandler(vars, schema)
        # self._validate_modules()
        self._validate_schema()

    # def _validate_modules(self):
    #     if self.log.has_errors():
    #         return

    #     for name, schema in self.vars.get_schema():
    #         var = self.vars.get_var(name)
    #         entry = TypeValidator.create(name, var, schema)
    #         if entry:
    #             entry.validate_modules()

    def _validate_schema(self):
        if self.log.has_errors():
            return

        for name, schema in self.vars.get_schema():
            var = self.vars.get_var(name)
            entry = TypeValidator.create(name, var, schema)
            if entry:
                entry.validate_schema()

    def validate(self):
        if self.log.has_errors():
            return

        for name, schema in self.vars.get_schema():
            var = self.vars.get_var(name)
            entry = TypeValidator.create(name, var, schema)
            if entry:
                entry.validate()

    def document(self):
        if self.log.has_errors():
            return

        for name, schema in self.vars.get_schema():
            var = self.vars.get_var(name)
            entry = TypeValidator.create(name, var, schema)
            if entry:
                self.doc.add_entry(name, entry.get_documentation())

    def is_valid(self):
        if self.log.has_errors():
            return False
        return True

    def get_errors(self):
        return self.log.all_errors()

    def get_error_message(self):
        return self.log.error_message()

    def get_documentation(self):
        doc = self.doc.generate()
        return doc
