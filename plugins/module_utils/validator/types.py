from ansible_collections.gxmmx.utility.plugins.module_utils.validator.handlers import LogHandler
from ansible_collections.gxmmx.utility.plugins.module_utils.validator.functions import ValidatorRegex

from datetime import datetime
import ipaddress
import json
import re

# ------------------------------------------------------------------------------
# Type validator
# ------------------------------------------------------------------------------

class TypeValidator:
    """
    The top level class that handles entry validation of variables based on a schema.
    Is called by Validator to create an entry for each variable in the schema.
    """
    @staticmethod
    def create(name, var, schema):
        log = LogHandler()
        if not isinstance(schema, dict):
            log.error(f"Validation schema entry not a valid dictionary", name)
            return None

        schema_type = schema.get('type')
        if schema_type is None:
            log.error(f"Validation schema entry missing type field", name)
            return None
        for cls in BaseType.__subclasses__():
            if cls.type_name == schema_type:
                return cls(name, var, schema)
        log.error(f"Validation schema not available for type '{schema_type}'", name)
        return None

# ------------------------------------------------------------------------------
# Base validator
# ------------------------------------------------------------------------------

class BaseType:
    """
    Base class for type validators
    """
    ## Type name.
    #  Must be set in child classes
    type_name = None

    ## Init type
    #  Initializes each type. Subclassess should set attributes after calling super.
    def __init__(self, name, var, schema):
        self.name = str(name)
        self.var = var
        self.schema = dict(schema)
        self.log = LogHandler()
        self.default_attributes = {
            'doc': dict,
            'optional': bool,
            'default': str
        }
        self.required_attributes = {}
        self.optional_attributes = {}

        self.doc_data = {}
        self.doc_default = ""


    ## Type specific validation
    #  Must be overwritten in child classes
    def validation(self):
        raise Exception("Type is missing validation function.")


    ## Type specific documentation
    #  Must be overwritten in child classes
    def documentation(self):
        raise Exception("Type is missing documentation function.")


    ## Type specific schema validation
    #  Can be overwritten in child classes
    def schema_validation(self):
        pass


    ## Main schema validation
    # Called by validator
    def validate_schema(self):
        # Check required attributes
        missing = set(self.required_attributes.keys()) - set(self.schema.keys())
        if missing:
            self.log.error(f"Validation schema missing required attributes {missing}", self.name)
            return None

        # Check for any unknown attributes
        all_allowed = set(self.required_attributes.keys()) | set(self.optional_attributes.keys()) | set(self.default_attributes.keys())
        unknown = set(self.schema.keys()) - set(all_allowed) - set({'type'})
        if unknown:
            self.log.error(f"Validation schema contains unknown attributes {unknown}", self.name)
            return None

        # Check attribute types
        all_attributes = self.default_attributes.copy()
        all_attributes.update(self.required_attributes)
        all_attributes.update(self.optional_attributes)
        for key, value in self.schema.items():
            expected_key_type = all_attributes.get(key)
            if expected_key_type and not isinstance(value, expected_key_type):
                self.log.error(f"Validation schema attribute '{key}' should be {expected_key_type.__name__}, got {type(value).__name__}", self.name)
                return None
        # Doc schema
        if self.schema.get('doc'):
            unknown = set(self.schema.get('doc')) - set({'order', 'description', 'default', 'active'})
            if unknown:
                self.log.error(f"Validation schema contains unknown attributes for doc {unknown}", self.name)
                return None

            if 'order' in self.schema['doc']:
                if not isinstance(self.schema['doc']['order'], int):
                    self.log.error(f"Validation schema attribute 'doc.order' must be integer", self.name)
                    return None
            if 'active' in self.schema['doc']:
                if not isinstance(self.schema['doc']['active'], bool):
                    self.log.error(f"Validation schema attribute 'doc.active' must be boolean", self.name)
                    return None

        # Perform type specific schema validation
        self.schema_validation()


    ## Main validation
    #  Checks if value is required or optional and runs type specific validation
    def validate(self):
        # Check optional
        if self.var is None:
            if not bool(self.schema.get('optional')):
                self.log.error('Missing required variable', self.name)
            return None

        # Perform type specific validation
        self.validation()


    def doc_add_param(self, param: dict):
        self.doc_data.setdefault('parameters', [])
        self.doc_data['parameters'].append(param)

    def get_documentation(self):

        # Defaults
        self.doc_data['name'] = self.name
        self.doc_data['value'] = self.doc_default
        self.doc_data['type'] = self.type_name if self.type_name is not None else ""
        self.doc_data['active'] = True
        self.doc_data['order'] = 999
        self.doc_data['description'] = []

        # Doc
        if self.schema.get('doc'):
            # Order
            if 'order' in self.schema['doc']:
                self.doc_data['order'] = int(self.schema['doc']['order'])

            # Description
            if 'description' in self.schema['doc']:
                desc_parts = str(self.schema['doc']['description']).split('/')
                self.doc_data['description'] = [ line.strip() for line in desc_parts ]

            # Active
            if 'active' in self.schema['doc']:
                self.doc_data['active'] = bool(self.schema['doc']['active'])

            # Default
            if 'default' in self.schema['doc'] and self.schema['doc']['default']:
                if self.schema['doc']['default'] == "value":
                    if self.var is not None:
                        self.doc_data['value'] = self.var
                else:
                    self.doc_data['value'] = self.schema['doc']['default']
        # Parameters
        self.doc_data.setdefault('parameters', [])
        if self.schema.get('optional'):
            self.doc_add_param({'Optional': 'true'})

        # Type specific documentaion
        self.documentation()
        return self.doc_data


# ------------------------------------------------------------------------------
# List validator
# ------------------------------------------------------------------------------

class ListType(BaseType):
    """
    Validates variables of type: list
    Calls recursive subschema validators for list items.
    """
    type_name = "list"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "schema"
        self.required_attributes = {
            'schema': dict
        }
        self.optional_attributes = {
            'empty': bool
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})

        name = "item"
        item = TypeValidator.create(name, None, self.schema.get('schema'))
        if item:
            doc = item.get_documentation()
            self.doc_data[name] = doc

    def schema_validation(self):
        name = f"{self.name}[item]"
        item = None
        entry = TypeValidator.create(name, item, self.schema.get('schema'))
        if entry:
            entry.validate_schema()

    def validation(self):
        # Check type
        if not isinstance(self.var, list):
            self.log.error(f"Not of type 'list'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty list", self.name)
                return
        # Create validator for each item in list
        for item in self.var:
            name = f"{self.name}[item]"
            entry = TypeValidator.create(name, item, self.schema.get('schema'))
            if entry:
                entry.validate()


# ------------------------------------------------------------------------------
# Dict validator
# ------------------------------------------------------------------------------

class DictionaryType(BaseType):
    """
    Validates variables of type: dictionary
    Calls recursive subschema validators for dictionary items.
    """
    type_name = "dictionary"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "schema"
        self.required_attributes = {
            'schema': dict
        }
        self.optional_attributes = {
            'empty': bool,
            'unknown': bool,
            'one_of': list
        }


    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('unknown'):
            self.doc_add_param({'Allow unknown keys': 'true'})
        if self.schema.get('one_of'):
            self.doc_add_param({'Requires one of': f"{set(self.schema.get('one_of'))}"})

        subschema: dict = self.schema.get('schema')
        for name, schema in subschema.items():
            item = TypeValidator.create(name, None, schema)
            if item:
                doc = item.get_documentation()
                self.doc_data.setdefault('keys', {})
                self.doc_data['keys'][name] = doc

    def schema_validation(self):
        # Set subschema for parsing
        subschema: dict = self.schema.get('schema')

        # Validate one_of values
        if self.schema.get('one_of'):
            one_of_not_declared = set(self.schema.get('one_of')) - set(subschema.keys())
            if one_of_not_declared:
                self.log.error(f"Validation schema one_of contains undeclared keys {one_of_not_declared}", self.name)
                return

        # Create validator for each key in dict
        for name, schema in subschema.items():
            fullname = f"{self.name}.{name}"
            var = None
            entry = TypeValidator.create(fullname, var, schema)
            if entry:
                entry.validate_schema()

    def validation(self):
        # Set subschema for parsing
        subschema: dict = self.schema.get('schema')

        # Check type
        if not isinstance(self.var, dict):
            self.log.error(f"Not of type 'dictionary'", self.name)
            return

        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty dictionary", self.name)
                return

        # Check unknown keys
        unknown = set(self.var.keys()) - set(subschema.keys())
        if unknown:
            if not self.schema.get('unknown'):
                self.log.error(f"Dictionary contains unknown keys {unknown}", self.name)
                return
        
        # Check one_of keys
        if self.schema.get('one_of'):
            one_of = set(self.schema.get('one_of')).intersection(set(self.var.keys()))
            if not one_of:
                self.log.error(f"Dictionary must containone of {set(self.schema.get('one_of'))}", self.name)
                return

        # Create validator for each key in dict
        for name, schema in subschema.items():
            fullname = f"{self.name}.{name}"
            var = self.var.get(name)
            entry = TypeValidator.create(fullname, var, schema)
            if entry:
                entry.validate()


# ------------------------------------------------------------------------------
# Boolean validator
# ------------------------------------------------------------------------------

class BooleanType(BaseType):
    """
    Validates variables of type: boolean
    """
    type_name = "boolean"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "true"
        self.required_attributes = {}
        self.optional_attributes = {}

    def documentation(self):
        pass

    def validation(self):
        # TODO: Check allow string representation of true/false/yes/no?

        # Check type
        if not isinstance(self.var, bool):
            self.log.error(f"Not of type 'boolean'", self.name)


# ------------------------------------------------------------------------------
# Integer validator
# ------------------------------------------------------------------------------

class IntegerType(BaseType):
    """
    Validates variables of type: integer
    """
    type_name = "integer"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "1"
        self.required_attributes = {}
        self.optional_attributes = {
            'min_value': int,
            'max_value': int,
            'values': list
        }

    def documentation(self):
        if self.schema.get('min_value'):
            self.doc_add_param({'Min value': self.schema.get('min_value')})
        if self.schema.get('max_value'):
            self.doc_add_param({'Max value': self.schema.get('max_value')})
        if self.schema.get('values'):
            self.doc_add_param({'Allowed values': ','.join(str(v) for v in self.schema.get('values'))})

    def schema_validation(self):
        if self.schema.get('min_value') and self.schema.get('max_value'):
            if self.schema.get('min_value') >= self.schema.get('max_value'):
                self.log.error(f"Validation schema min value not less than max", self.name)
        if self.schema.get('values'):
            for value in self.schema.get('values'):
                if not isinstance(value, int):
                    self.log.error(f"Validation schema values list should only contain integers", self.name)
                    return

    def validation(self):
        # Check type
        if not isinstance(self.var, int):
            self.log.error(f"Not of type 'int'", self.name)
            return
        # Check allowed values
        if self.schema.get('values'):
            if self.var not in self.schema.get('values'):
                self.log.error(f"Allowed values: {self.schema.get('values')}", self.name)
                return
        # Check min value
        if self.schema.get('min_value'):
            if self.var < self.schema.get('min_value'):
                self.log.error(f"Min value: {self.schema.get('min_value')}", self.name)
        # Check max value
        if self.schema.get('max_value'):
            if self.var > self.schema.get('max_value'):
                self.log.error(f"Max value: {self.schema.get('max_value')}", self.name)


# ------------------------------------------------------------------------------
# String validator
# ------------------------------------------------------------------------------

class StringType(BaseType):
    """
    Validates variables of type: string
    """
    type_name = "string"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "string"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'min_length': int,
            'max_length': int,
            'starts_with': str,
            'ends_with': str,
            'regex': str,
            'values': list
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('min_length'):
            self.doc_add_param({'Min length': self.schema.get('min_length')})
        if self.schema.get('max_length'):
            self.doc_add_param({'Max length': self.schema.get('max_length')})
        if self.schema.get('starts_with'):
            self.doc_add_param({'Must start with': self.schema.get('starts_with')})
        if self.schema.get('ends_with'):
            self.doc_add_param({'Must end with': self.schema.get('ends_with')})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})
        if self.schema.get('values'):
            self.doc_add_param({'Allowed values': ','.join(str(v) for v in self.schema.get('values'))})

    def schema_validation(self):
        if self.schema.get('min_length') and self.schema.get('max_length'):
            if self.schema.get('min_length') >= self.schema.get('max_length'):
                self.log.error(f"Validation schema min length not less than max", self.name)
        if self.schema.get('values'):
            for value in self.schema.get('values'):
                if not isinstance(value, str):
                    self.log.error(f"Validation schema values list should only contain strings", self.name)
                    return

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check allowed values
        if self.schema.get('values'):
            if self.var not in self.schema.get('values'):
                self.log.error(f"Allowed values: {self.schema.get('values')}", self.name)
                return
        # Check min length
        if self.schema.get('min_length'):
            if len(self.var) < self.schema.get('min_length'):
                self.log.error(f"Min length: {self.schema.get('min_length')}", self.name)
        # Check max length
        if self.schema.get('max_length'):
            if len(self.var) > self.schema.get('max_length'):
                self.log.error(f"Max length: {self.schema.get('max_length')}", self.name)
        # Check starts with
        if self.schema.get('starts_with'):
            if not self.var.startswith(self.schema.get('starts_with')):
                self.log.error(f"Required to start with: '{self.schema.get('starts_with')}'", self.name)
        # Check ends with
        if self.schema.get('ends_with'):
            if not self.var.endswith(self.schema.get('ends_with')):
                self.log.error(f"Required to end with: '{self.schema.get('ends_with')}'", self.name)
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)


# ------------------------------------------------------------------------------
# Path validator
# ------------------------------------------------------------------------------

class PathType(BaseType):
    """
    Validates variables of type: path
    """
    type_name = "path"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "/some/path"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'absolute': bool,
            'relative': bool,
            'file': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('absolute'):
            self.doc_add_param({'Path must be absolute': 'true'})
        if self.schema.get('relative'):
            self.doc_add_param({'Path must be relative': 'true'})
        if self.schema.get('file'):
            self.doc_add_param({'Must be filename': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def schema_validation(self):
        if self.schema.get('absolute') and self.schema.get('relative'):
            self.log.error(f"Validation schema can't be absolute and relative", self.name)
        if self.schema.get('absolute') and self.schema.get('file'):
            self.log.error(f"Validation schema can't be absolute and file", self.name)
        if self.schema.get('relative') and self.schema.get('file'):
            self.log.error(f"Validation schema can't be relative and file", self.name)

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check absolute
        if self.schema.get('absolute'):
            if not self.var.startswith('/'):
                self.log.error(f"Not an absolute path'", self.name)
                return
        # Check relative
        if self.schema.get('relative'):
            if self.var.startswith('/'):
                self.log.error(f"Not an relative path'", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return
        # Check path
        r = ValidatorRegex()
        if self.schema.get('file'):
            format_file = fr"^{r.file}$"
            if re.match(format_file, self.var) is None:
                self.log.error(f"Invalid file name", self.name)
        else:
            format_path = fr"^{r.path}$"
            if re.match(format_path, self.var) is None:
                self.log.error(f"Invalid path", self.name)


# ------------------------------------------------------------------------------
# Hostname validator
# ------------------------------------------------------------------------------

class HostnameType(BaseType):
    """
    Validates variables of type: hostname
    """
    type_name = "hostname"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "localhost"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'fqdn': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('fqdn'):
            self.doc_add_param({'Allow FQDN': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return

        # Check hostname
        r = ValidatorRegex()
        format_hostname = fr"^{r.hostname}$"
        format_fqdn = fr"^{r.hostname}{r.domain}$"

        if re.match(format_hostname, self.var) is None:
            if self.schema.get('fqdn'):
                if re.match(format_fqdn, self.var) is not None:
                    return
            self.log.error(f"Invalid hostname", self.name)


# ------------------------------------------------------------------------------
# Domain validator
# ------------------------------------------------------------------------------

class DomainType(BaseType):
    """
    Validates variables of type: domain
    """
    type_name = "domain"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "domain.com"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'host': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('host'):
            self.doc_add_param({'Allow simple hostname': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return

        # Check domain name
        r = ValidatorRegex()
        format_hostname = fr"^{r.hostname}$"
        format_fqdn = fr"^{r.hostname}{r.domain}$"

        if re.match(format_fqdn, self.var) is None:
            if self.schema.get('host'):
                if re.match(format_hostname, self.var) is not None:
                    return
            self.log.error(f"Invalid domain name", self.name)


# ------------------------------------------------------------------------------
# Email validator
# ------------------------------------------------------------------------------

class EmailType(BaseType):
    """
    Validates variables of type: email
    """
    type_name = "email"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "email@domain.com"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return

        # Check email address
        r = ValidatorRegex()
        format_email = fr"^{r.email_prefix}@{r.hostname}{r.domain}$"

        if re.match(format_email, self.var) is None:
            self.log.error(f"Invalid email address", self.name)


# ------------------------------------------------------------------------------
# Url validator
# ------------------------------------------------------------------------------

class UrlType(BaseType):
    """
    Validates variables of type: url
    """
    type_name = "url"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "https://domain.com"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return
        # Check url string
        r = ValidatorRegex()
        format_url = fr"^{r.url_proto}({r.hostname}{r.domain}|{r.url_ipv4}|{r.url_ipv6}){r.url_port}{r.url_path}{r.url_query}{r.url_fragment}$"

        if re.match(format_url, self.var) is None:
            self.log.error(f"Invalid url", self.name)


# ------------------------------------------------------------------------------
# Json validator
# ------------------------------------------------------------------------------

class JsonType(BaseType):
    """
    Validates variables of type: json
    """
    type_name = "json"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = r'{"key": "value"}'
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return
        # Check json string
        try:
            json.loads(self.var)
        except json.JSONDecodeError:
            self.log.error(f"Invalid json", self.name)


# ------------------------------------------------------------------------------
# Datetime validator
# ------------------------------------------------------------------------------

class DatetimeType(BaseType):
    """
    Validates variables of type: datetime
    """
    type_name = "datetime"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "2024-01-01"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'format': str,
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('format'):
            self.doc_add_param({'Must match format': f"{self.schema.get('format')}"})

    def validation(self):
        # Check type
        if isinstance(self.var, str):
            dt = self.var
        elif isinstance(self.var, (int, float)):
            dt = str(self.var)
        else:
            self.log.error(f"Not of type 'string' or 'float'", self.name)
            return
        # Check empty
        if len(dt) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return
        
        # Datetime validation
        dt_formats = [
            "%Y-%m-%d", "%Y-%m-%d %H:%M:%S", "%Y/%m/%d", "%Y/%m/%d %H:%M:%S",
            "%m-%d-%Y", "%m-%d-%Y %H:%M:%S", "%d-%m-%Y", "%d-%m-%Y %H:%M:%S",
            "%m/%d/%Y", "%m/%d/%Y %H:%M:%S", "%d/%m/%Y", "%d/%m/%Y %H:%M:%S",
            "%Y.%m.%d", "%Y.%m.%d %H:%M:%S", "%H:%M:%S"
        ]
        # If format is passed, try format
        if self.schema.get('format'):
            if self.schema.get('format') == 'unix':
                try:
                    if float(dt) and datetime.fromtimestamp(float(dt)):
                        return
                except:
                    self.log.error(f"Invalid datetime", self.name)
                    return
            try:
                datetime.strptime(dt, self.schema.get('format'))
                return
            except:
                self.log.error(f"Invalid datetime", self.name)
                return
        # If no format is passed, try all
        try:
            if float(dt) and datetime.fromtimestamp(float(dt)):
                return
        except:
            pass
        for dt_format in dt_formats:
            try:
                datetime.strptime(dt, dt_format)
                return True
            except:
                continue
        self.log.error(f"Invalid datetime", self.name)


# ------------------------------------------------------------------------------
# Ipaddress validator
# ------------------------------------------------------------------------------

class IpaddressType(BaseType):
    """
    Validates variables of type: ipaddress
    """
    type_name = "ipaddress"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "192.168.1.2"
        self.required_attributes = {}
        self.optional_attributes = {
            'empty': bool,
            'version': str,
            'require_mask': bool,
            'network': bool,
            'interface': bool,
            'range': bool,
            'range_separator': str,
            'values' : str
        }

    def documentation(self):
        if self.schema.get('empty'):
            self.doc_add_param({'Allow empty': 'true'})
        if self.schema.get('version'):
            if self.schema.get('version') in ['ipv4', 'ipv6']:
                self.doc_add_param({'Version': self.schema.get('version')})
            else:
                self.doc_add_param({'Version': "ipv4 or ipv6"})
        else:
            self.doc_add_param({'Version': "ipv4 or ipv6"})
        if not self.schema.get('require_mask'):
            self.doc_add_param({'Accepts single ip': 'true'})
        if self.schema.get('network'):
            self.doc_add_param({'Accepts valid network': 'true'})
        if self.schema.get('interface'):
            self.doc_add_param({'Accepts valid interface': 'true'})
        if self.schema.get('range'):
            self.doc_add_param({'Accepts valid ip range': 'true'})
            if self.schema.get('range_separator'):
                self.doc_add_param({'Range separator': self.schema.get('range_separator')})
            else:
                self.doc_add_param({'Range separator': '-'})
        if self.schema.get('values'):
            self.doc_add_param({'Allowed values': ','.join(str(v) for v in self.schema.get('values'))})

    def schema_validation(self):
        if self.schema.get('version'):
            if self.schema.get('version') not in ['ipv4', 'ipv6', 'any']:
                self.log.error("Validation schema version must be in [ipv4,ipv6,any]", self.name)
        if self.schema.get('require_mask') and self.schema.get('range'):
            self.log.error("Validation schema can not accept range and require mask", self.name)
        if self.schema.get('require_mask') and not self.schema.get('network') and not self.schema.get('interface'):
            self.log.error("Validation schema can not require mask if not network or interface", self.name)
        if self.schema.get('values'):
            for value in self.schema.get('values'):
                if not isinstance(value, str):
                    self.log.error(f"Validation schema values list should only contain strings", self.name)
                    return

    def validate_ip(self, value: str, version=None, require_mask=None, network=None, interface=None, range=None, range_separator=None):
        if range:
            sep = '-'
            if range_separator:
                sep = range_separator
            parts = value.split(sep)
            if len(parts) > 2:
                    self.log.error(f"Invalid IP range", self.name)
                    return None
            elif len(parts) == 2:
                range_fr = self.validate_ip(parts[0], version, False, False, False, False, None)
                if not range_fr:
                    return None
                range_to = self.validate_ip(parts[1], version, False, False, False, False, None)
                if not range_to:
                    return None
                range_fr_ip = ipaddress.ip_address(range_fr)
                range_to_ip = ipaddress.ip_address(range_to)
                if not range_fr_ip < range_to_ip:
                    self.log.error(f"Invalid IP range", self.name)
                    return None
                if not range_fr_ip.version == range_to_ip.version:
                    self.log.error(f"Invalid IP range", self.name)
                    return None
                return value

        found = False
        if not require_mask:
            try:
                addr = ipaddress.ip_address(value)
                found = True
            except:
                pass
        if not found and network and '/' in value:
            try:
                addr = ipaddress.ip_network(value)
                found = True
            except:
                pass
        if not found and interface and '/' in value:
            try:
                addr = ipaddress.ip_interface(value)
                found = True
            except:
                pass

        if not found:
            self.log.error(f"Invalid IP", self.name)
            return None

        if version:
            if version == "ipv4":
                if 4 != addr.version:
                    self.log.error(f"Invalid IPv4", self.name)
                    return None
            if version == "ipv6":
                if 6 != addr.version:
                    self.log.error(f"Invalid IPv6", self.name)
                    return None
        return value

    def validation(self):
         # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check empty
        if len(self.var) == 0:
            if self.schema.get('empty'):
                return
            else:
                self.log.error(f"Empty string", self.name)
                return

        # Check ipaddress format
        _ = self.validate_ip(
            self.var, self.schema.get('version'),
            self.schema.get('require_mask'),
            self.schema.get('network'),
            self.schema.get('interface'),
            self.schema.get('range'),
            self.schema.get('range_separator')
        )

        # Check allowed values
        if self.schema.get('values'):
            if self.var not in self.schema.get('values'):
                self.log.error(f"Allowed values: {self.schema.get('values')}", self.name)
                return


# ------------------------------------------------------------------------------
# Port validator
# ------------------------------------------------------------------------------

class PortType(BaseType):
    """
    Validates variables of type: port
    """
    type_name = "port"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "8080/tcp"
        self.required_attributes = {}
        self.optional_attributes = {
            'min_value': int,
            'max_value': int,
            'type_only': bool,
            'number_only': bool,
            'require_type': bool,
            'range': bool,
            'range_separator': str,
            'values': list
        }

    def documentation(self):
        if self.schema.get('min_value'):
            self.doc_add_param({'Min value': self.schema.get('min_value')})
        if self.schema.get('max_value'):
            self.doc_add_param({'Max value': self.schema.get('max_value')})
        if self.schema.get('require_type'):
            self.doc_add_param({'Port type required': self.schema.get('require_type')})

        if self.schema.get('range'):
            self.doc_add_param({'Accepts valid port range': 'true'})
            if self.schema.get('range_separator'):
                self.doc_add_param({'Range separator': self.schema.get('range_separator')})
            else:
                self.doc_add_param({'Range separator': '-'})
        if self.schema.get('values'):
            self.doc_add_param({'Allowed values': ','.join(str(v) for v in self.schema.get('values'))})


    def schema_validation(self):
        if self.schema.get('type_only') and self.schema.get('require_type'):
            self.log.error("Validation schema can not be type_only and require_type", self.name)
        if self.schema.get('type_only') and self.schema.get('range'):
            self.log.error("Validation schema can not be type_only and accept range", self.name)
        if self.schema.get('type_only') and self.schema.get('values'):
            self.log.error("Validation schema can not be type_only and accept values", self.name)
        if self.schema.get('type_only') and self.schema.get('min_value'):
            self.log.error("Validation schema can not be type_only and accept min_value", self.name)
        if self.schema.get('type_only') and self.schema.get('max_value'):
            self.log.error("Validation schema can not be type_only and accept max_value", self.name)
        if self.schema.get('type_only') and self.schema.get('number_only'):
            self.log.error("Validation schema can not be type_only and accept number_only", self.name)
        if self.schema.get('number_only') and self.schema.get('require_type'):
            self.log.error("Validation schema can not be number_only and require_type", self.name)


        if self.schema.get('min_value'):
            if self.schema.get('min_value') < 1:
                self.log.error(f"Validation schema min value must not be less than 1", self.name)
        if self.schema.get('max_value'):
            if self.schema.get('max_value') > 65535:
                self.log.error(f"Validation schema max value must not be greater than 65535", self.name)
        if self.schema.get('min_value') and self.schema.get('max_value'):
            if self.schema.get('min_value') >= self.schema.get('max_value'):
                self.log.error(f"Validation schema min value not less than max", self.name)
        if self.schema.get('range_separator'):
            if self.schema.get('range_separator') == "/":
                self.log.error(f"Validation schema range separator can not be type separator '/'", self.name)
        if self.schema.get('values'):
            for value in self.schema.get('values'):
                if not isinstance(value, int):
                    self.log.error(f"Validation schema values list should only contain integers", self.name)
                    return

    def validate_port(self, value: str, number_only=None, require_type=None, range=None, range_separator=None):

        port_and_type = value.split('/')
        if len(port_and_type) > 2:
            self.log.error(f"Invalid port", self.name)
            return None
        elif len(port_and_type) == 2:
            port_value = port_and_type[0]
            port_type = port_and_type[1]

        else:
            port_value = port_and_type[0]
            port_type = ""

        if require_type and not port_type:
            self.log.error(f"Invalid port, missing type declaration '/tcp|udp'", self.name)
            return None

        if number_only and port_type:
            self.log.error(f"Invalid port, must be port number only", self.name)
            return None

        if port_type and port_type not in ['tcp', 'udp']:
            self.log.error(f"Invalid port, type declaration must be in '[tcp,udp]'", self.name)
            return None

        if range:
            sep = '-'
            if range_separator:
                sep = range_separator
            parts = port_value.split(sep)
            if len(parts) > 2:
                    self.log.error(f"Invalid port range", self.name)
                    return None
            elif len(parts) == 2:
                range_fr = self.validate_port(parts[0], True, False, False, None)
                range_to = self.validate_port(parts[1], True, False, False, None)
                if not range_fr:
                    return None
                if not range_to:
                    return None
                if not range_fr < range_to:
                    self.log.error(f"Invalid port range", self.name)
                return None

        try:
            port_int = int(port_value)
        except:
            self.log.error(f"Invalid port", self.name)
            return None

        if not 1 <= port_int <= 65535:
            self.log.error(f"Invalid port, outside valid range", self.name)

        return port_int

    def validation(self):
         # Check instance type
        if isinstance(self.var, str):
            var = self.var
        elif isinstance(self.var, int):
            var = str(self.var)
        else:
            self.log.error(f"Not of type 'string' or 'integer'", self.name)
            return

        # Check type only
        if self.schema.get('type_only'):
            if var not in ['tcp', 'udp']:
                self.log.error(f"Port type only must be in ['tcp','udp']", self.name)
            return

        # Check valid port
        _ = self.validate_port(
            var,
            self.schema.get('number_only'),
            self.schema.get('require_type'),
            self.schema.get('range'),
            self.schema.get('range_separator')
        )

        # Check allowed values
        if self.schema.get('values'):
            if var not in self.schema.get('values'):
                self.log.error(f"Allowed values: {self.schema.get('values')}", self.name)
                return


# ------------------------------------------------------------------------------
# Certificate validator
# ------------------------------------------------------------------------------

class CertificateType(BaseType):
    """
    Validates variables of type: certificate
    """
    type_name = "certificate"

    def __init__(self, name, var, schema):
        super().__init__(name, var, schema)
        self.doc_default = "-----BEGIN CERTIFICATE-----"
        self.required_attributes = {}
        self.optional_attributes = {
            'type': str,
            'pem': str,
            'newline': str,
            'regex': str
        }

    def documentation(self):
        if self.schema.get('pem_type'):
            self.doc_add_param({'Accepted': f"{self.schema.get('pem_type')}"})
        else:
             self.doc_add_param({'Accepted': "certificate,request,key"})
        if self.schema.get('newline'):
            if self.schema.get('newline') == 'require':
                self.doc_add_param({'Newline': "required"})
            elif self.schema.get('newline') == 'deny':
                self.doc_add_param({'Newline': "not allowed"})
            else:
                self.doc_add_param({'Newline': "allowed"})
        else:
            self.doc_add_param({'Newline': "allowed"})
        if self.schema.get('regex'):
            self.doc_add_param({'Must match': f"{self.schema.get('regex')}"})

    def schema_validation(self):
        if self.schema.get('pem'):
            valid_vals = ['certificate', 'key', 'request']
            if not all(item.strip() in valid_vals for item in self.schema.get('pem').split(',')):
                self.log.error("Validation schema for pem must be in comma separated list of 'certificate, request, key'", self.name)
        if self.schema.get('newline'):
            if self.schema.get('newline') not in ['allow', 'deny', 'require']:
                self.log.error("Validation schema for newline must be in 'allow, deny, require'", self.name)


    def is_valid_pem_content(self, content, pem_patterns):
        for header, footer in pem_patterns:
            if self.schema.get('newline') == 'require':
                pattern = re.compile(r"^" + re.escape(header) + r"[A-Za-z0-9+/=\s]+" + re.escape(footer) + r"\n" + r"\Z")
            elif self.schema.get('newline') == 'deny':
                pattern = re.compile(r"^" + re.escape(header) + r"[A-Za-z0-9+/=\s]+" + re.escape(footer) + r"\Z")
            else:
                pattern = re.compile(r"^" + re.escape(header) + r"[A-Za-z0-9+/=\s]+" + re.escape(footer) + r"$")
            if re.search(pattern, content):
                return True
        return False


    def valid_certificate(self):
        pem_patterns = [
            (r"-----BEGIN CERTIFICATE-----", r"-----END CERTIFICATE-----"),
        ]
        return self.is_valid_pem_content(self.var, pem_patterns)

    def valid_request(self):
        pem_patterns = [
            (r"-----BEGIN CERTIFICATE REQUEST-----", r"-----END CERTIFICATE REQUEST-----"),
        ]
        return self.is_valid_pem_content(self.var, pem_patterns)

    def valid_key(self):
        pem_patterns = [
            (r"-----BEGIN PRIVATE KEY-----", r"-----END PRIVATE KEY-----"),
            (r"-----BEGIN RSA PRIVATE KEY-----", r"-----END RSA PRIVATE KEY-----"),
        ]
        return self.is_valid_pem_content(self.var, pem_patterns)


    def validation(self):
        # Check type
        if not isinstance(self.var, str):
            self.log.error(f"Not of type 'string'", self.name)
            return
        # Check regex
        if self.schema.get('regex'):
            if not re.match(self.schema.get('regex'), self.var):
                self.log.error(f"Required to match regex: '{self.schema.get('regex')}'", self.name)
                return

        # Check pem contents
        if self.schema.get('pem'):
            pem = self.schema.get('pem')
        else:
            pem = 'certificate,request,key'

        pem_list = pem.split(',')
        found = False
        if 'certificate' in pem_list:
            if self.valid_certificate():
                found = True
        if not found and 'request' in pem_list:
            if self.valid_request():
                found = True
        if not found and 'key' in pem_list:
            if self.valid_key():
                found = True

        if not found:
            if len(pem_list) == 1:
                self.log.error(f"Invalid pem {pem_list[0]}", self.name)
            else:
                self.log.error(f"Invalid pem content", self.name)
