from secrets import choice
import string

# ------------------------------------------------------------------------------
# Token Generator
# ------------------------------------------------------------------------------

class TokenGenerator:
    """
    Token generation class.
    Generates token of desired length and format.
    """

    character_sets = {
        'number': [string.digits],
        'lower': [string.ascii_lowercase],
        'upper': [string.ascii_uppercase],
        'alpha': [string.ascii_lowercase, string.ascii_uppercase],
        'alnum': [string.ascii_lowercase, string.ascii_uppercase, string.digits],
        'uppnum': [string.ascii_uppercase, string.digits],
        'lownum': [string.ascii_lowercase, string.digits],
    }

    def __init__(self, length: int, format: str='alnum', prefix: str=''):
        self.length = length
        self.format = format
        self.prefix = prefix
        self.token = ''
        self._generate()


    def _generate(self):
        chars = ''.join(''.join(subset) for subset in self.character_sets[self.format])
        while True:
            token = ''.join(choice(chars) for _ in range(self.length))
            if len(self.character_sets[self.format]) > 1:
                # Check for at least one character from each subset
                if all(any(c in subset for c in token) for subset in self.character_sets[self.format]):
                    self.token = token
                    return
            else:
                self.token = token
                return

    def value(self):
        return f"{self.prefix}{self.token}"
