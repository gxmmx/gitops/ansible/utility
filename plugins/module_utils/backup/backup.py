import os
import re
import shutil

# ------------------------------------------------------------------------------
# Backup Files
# ------------------------------------------------------------------------------

class BackupFiles:
    """
    Backup file manipulation.
    Fetches all backups for a specified file.
    Can restore backups and ensure rotation of older backups.
    """

    def __init__(self, path: str, check_mode: bool=False):
        self.path = os.path.expanduser(path)
        self.check_mode = check_mode
        self.backups = []
        self._fetch_backups()


    def _fetch_backups(self):
        ansible_backup_regex = r'\d{2}-\d{2}-\d{2}@\d{2}:\d{2}:\d{2}~$'

        if not os.path.isfile(self.path):
            raise FileNotFoundError(f"File does not exist: '{self.path}'")

        dir = os.path.dirname(self.path)
        filename = os.path.basename(self.path)

        files_in_dir = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        backup_files = [file for file in files_in_dir if str(file).startswith(filename) and re.search(ansible_backup_regex, file)]
        backup_tuples = []
        for file in backup_files:
            dtstr = str(file).rsplit('.', 1)[-1]
            dt = dtstr.translate({ord(i): None for i in '-~:@'})
            item = (os.path.join(dir, file), int(dt))
            backup_tuples.append(item)
        sorted_backup_tuples = sorted(backup_tuples, key=lambda x: x[1], reverse=True)
        self.backups = [os.path.join(dir, file[0]) for file in sorted_backup_tuples]


    def restore(self, file: str='none') -> str:
        if file == "none":
            return None
        elif file == 'latest':
            filename = self.backups[0]
            if not self.check_mode:
                try:
                    shutil.copyfile(filename, self.path)
                    os.remove(filename)
                except:
                    raise Exception(f"Could not restore backup: {filename}")
            self.backups.remove(filename)
            return str(filename)
        elif file in self.backups:
            filename = file
            if not self.check_mode:
                try:
                    shutil.copyfile(filename, self.path)
                    os.remove(filename)
                except:
                    raise Exception(f"Could not restore backup: {filename}")
            self.backups.remove(filename)
            return str(filename)
        else:
            raise Exception(f"Backup file does not exist: {file}")


    def cleanup(self, keep: int=5) -> int:
        remove = []
        if len(self.backups) > keep:
            remove = self.backups[keep:]
            for file in remove:
                if not self.check_mode:
                    try:
                        os.remove(file)
                    except:
                        raise Exception(f"Could not remove backup: {file}")
            return len(remove)
        return 0
